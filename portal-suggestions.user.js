// ==UserScript==
// @id             iitc-plugin-pending-portals@stilgar
// @name           IITC plugin: Pending portals
// @version        0.0.1.@@DATETIMEVERSION@@
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      @@UPDATEURL@@
// @downloadURL    @@DOWNLOADURL@@
// @description    [@@BUILDNAME@@-@@BUILDDATE@@] List pending(submitted but not online) portals als listed on portalmap.projekt2k.de
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// @grant          none
// ==/UserScript==


function wrapper() {
	// ensure plugin framework is there, even if iitc is not yet loaded
	if(typeof window.plugin !== 'function') window.plugin = function() {};

	// PLUGIN START ////////////////////////////////////////////////////////

	// use own namespace for plugin
	window.plugin.portalSuggestions = function() {};

	window.plugin.portalSuggestions.markerLayer = {};
	window.plugin.portalSuggestions.markerLayerGroup = new L.LayerGroup();

	window.plugin.portalSuggestions.baseurl = 'http://portalmap.projekt2k.de/';

	window.plugin.portalSuggestions.icons = {
			0: 'point.png',
			1: 'point.png',
			2: 'point.png',
			3: 'point_rejected.png'
		};

	window.plugin.portalSuggestions.status = {
			0: 'pending',
			1: 'accepted',
			2: 'online',
			3: 'rejected'
		};

	/**
	* fetch portalsuggestions for the current view
	* from portalmap.projekt2k.de
	* this is user-triggere
	*/
	window.plugin.portalSuggestions.showSuggestions = function()
	{
		var mapBounds = map.getBounds();
		var requestData = {
			bound : {
				northEastLat : mapBounds._northEast.lat,
				northEastLong : mapBounds._northEast.lng,
				southWestLat : mapBounds._southWest.lat,
				southWestLong : mapBounds._southWest.lng
			},
			filter: false 
		};

		var url = '//portalmap.projekt2k.de/index.php?ajaxIitc&getPortals&ajax';

		$.ajax({
			url: url,
			data: requestData,
			type: 'POST',
			jsonpCallback: 'jsonCallback',
			contentType: "application/json",
			dataType: 'jsonp',
			success: function(json) {
				plugin.portalSuggestions.processResponse(json);
			}
		});
	}

	/**
	* process the response, add markers
	* 
	* @param object data
	*/
	window.plugin.portalSuggestions.processResponse = function(data)
	{
		if(window.plugin.portalSuggestions.markerLayerGroup !== false) {
			window.plugin.portalSuggestions.markerLayerGroup.clearLayers();
		}

		window.plugin.portalSuggestions.markerLayerGroup.addLayer(L.geoJson(data, {
			pointToLayer: function (feature, latlng) {
				var featureIcon;
				var iconKey = 0;

				if(typeof feature.properties.pStat != 'undefined') {
					iconKey = feature.properties.pStat;
				}

				featureIcon = L.icon( {
					iconUrl: window.plugin.portalSuggestions.baseurl+'img/'+window.plugin.portalSuggestions.icons[iconKey],
					iconSize: [29, 29]
				});

				var p = L.marker(latlng, {icon: featureIcon});

				p.on('click', window.plugin.portalSuggestions.clickHandler);

				return p;
			}
		}));
	}

	/**
	 * handler for clicks on a marker
	 * @param  object data
	 */
	window.plugin.portalSuggestions.clickHandler = function(data) {
		unselectOldPortal();

		var details = {
			img :		window.plugin.portalSuggestions.baseurl+'images/'+data.target.feature.properties.image,
			imgTitle :	'title="a suggested Portal"',
			title :		escapeHtmlSpecialChars(data.target.feature.properties.title),
			nick :		escapeHtmlSpecialChars(data.target.feature.properties.uNick),
			status :	window.plugin.portalSuggestions.status[data.target.feature.properties.pStat],
			date :		data.target.feature.properties.date.substr(0, 10),
			adNote :	'This portal is not (yet) online.<br />'
				+ 'Add your own suggestions on <a href="http://portalmap.projekt2k.de" target="_blank">the portalmap</a>.<br />'
				+ 'See <a href="http://portalmap.projekt2k.de/?start&howto" target="_blank">HowTo</a> for details.'
		};

		$('#portaldetails')
			.attr('class', 'psNeutral')
			.html(''
				+ '<h3 class="title">' + details.title + '</h3>'
				+ '<span class="close" onclick="unselectOldPortal();" title="Close">X</span>'
				// help cursor via ".imgpreview img"
				+ '<div class="imgpreview" ' + details.imgTitle + ' style="background-image: url(' + details.img + ')">'
				+ '<img class="hide" src="' + details.img + '"/>'
				+ '</div>'
				+ '<table id="randdetails"><tbody>'
				+ '<tr><td>' + details.nick + '</td><th>owner</th><th>added</th><td>' + details.date + '</td></tr>'
				+ '<tr><td>' + details.status + '</td><th>Status</th><th></th><td></td></tr>'
				+ '</tbody></table>'
				+ '<div class="linkdetails">' + details.adNote + '</div>'
				);
	}

	/**
	 * initialize everything needed for the plugin
	 */
	var setup = function() {

		window.addLayerGroup('Portal suggestions', window.plugin.portalSuggestions.markerLayerGroup, true);

		$('head').append('<style>/* styles for portalsuggestions*/\n\n.psNeutral {color:#ff7200;}\n.psNeutral .linkdetails {font-size:0.8em;}\n</style>');

		$('#toolbox').append(' <a id="toolShowSuggestions" title="Find all portal suggestions for this Area. see portalmap.projekt2k.de">Show  portal suggestions</a>');
		$('#toolShowSuggestions').on('click', plugin.portalSuggestions.showSuggestions);
	}

	// PLUGIN END //////////////////////////////////////////////////////////

	if(window.iitcLoaded && typeof setup === 'function') {
		setup();
	} else {
		if(window.bootPlugins)
			window.bootPlugins.push(setup);
		else
			window.bootPlugins = [setup];
	}
} // wrapper end
// inject code into site context
var script = document.createElement('script');
script.appendChild(document.createTextNode('('+ wrapper +')();'));
(document.body || document.head || document.documentElement).appendChild(script);
